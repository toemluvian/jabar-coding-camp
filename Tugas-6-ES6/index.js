// Soal 1

const luasPersegiPanjang = (panjang, lebar) => {
    return panjang * lebar;
};

let kelilingPersegiPanjang = (panjang, lebar) => {
    return 2 * (panjang + lebar);
};

console.log("Luas =", luasPersegiPanjang(10, 5));
console.log("Keliling =", kelilingPersegiPanjang(20, 5));


// Soal 2

const newFunction = (firstName, lastName) => {
    return {
        firstName, lastName,
        fullName: function () {
            console.log(`${firstName} ${lastName}`);
        },
    };
};

newFunction("William", "Imoh").fullName();


// Soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "Playing Football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);


// Soal 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

let combined = [...west, ...east];

console.log(combined);


// Soal 5

const planet = "earth";
const view = "glass";

let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(after);


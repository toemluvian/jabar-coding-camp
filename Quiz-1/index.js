// Soal 1
//Function Penghitung Jumlah Kata

//Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

//Contoh

//var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
//var kalimat_2 = " Saya Iqbal"
//var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

//jumlah_kata(kalimat_1) // 6
//jumlah_kata(kalimat_2) // 2
//jumlah_kata(kalimat_3) // 4

//*catatan
//Perhatikan double spasi di depan, belakang, maupun di tengah tengah kalimat

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

function jumlah_kata(kalimat) {
    console.log(kalimat.split(" ").filter(function (spasi) {
        return spasi != ""
    }).length);
}
jumlah_kata(kalimat_1)
jumlah_kata(kalimat_2)
jumlah_kata(kalimat_3)

// Soal 2
//Function Penghasil Tanggal Hari Esok

//Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

//contoh 1

//var tanggal = 29
//var bulan = 2
//var tahun = 2020

//next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

//contoh 2

//var tanggal = 28
//var bulan = 2
//var tahun = 2021

//next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

//contoh 3

//var tanggal = 31
//var bulan = 12
//var tahun = 2020

//next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

//Catatan :
//1. Dilarang menggunakan new Date()
//2. Perhatikan tahun kabisat

function next_date(tanggal, bulan, tahun) {
    arrayBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember",];
    if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12) {
        var jumlahHari = 31;
    } else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) {
        var jumlahHari = 30;
    } else if (bulan == 2) {
        if (tahun % 4 == 1 || (tahun % 100 == 0 && tahun % 4 == 1)) {
            var jumlahHari = 28;
        } else if (tahun % 4 == 0) {
            var jumlahHari = 29;
        }
    }
    if (tanggal == jumlahHari) {
        tanggal = 1;
        if (bulan == 12) {
            bulan = 1;
            tahun++;
        } else {
            bulan++;
        }
    }
    var stringBulan = arrayBulan[bulan - 1];
    console.log(tanggal, stringBulan, tahun);
}
// Contoh
var tanggal = 31;
var bulan = 12;
var tahun = 2021;

next_date(tanggal, bulan, tahun);
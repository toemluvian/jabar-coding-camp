// Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
for (var i = 0; i < (daftarHewan.length); i++) {
    console.log(daftarHewan[i])
}


// Soal 2

function introduce(data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + ", alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data);
console.log(perkenalan);


// Soal 3

function hitung_huruf_vokal(kalimat) {

    var kalimat = kalimat.toLowerCase();
    var kalimatArray = kalimat.split("");
    var hurufvokal = ["a", "i", "e", "o", "u"];

    var jumlah = 0;
    hurufvokal.forEach(hurufvokal => {
        kalimatArray.forEach(e => {
            e === hurufvokal ? jumlah += 1 : "";
        });
    });

    return jumlah;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2);


// Soal 4

function nama_hitung(angka) {
    return angka * 2 - 2;
}

console.log(nama_hitung(0))
console.log(nama_hitung(1))
console.log(nama_hitung(2))
console.log(nama_hitung(3))
console.log(nama_hitung(5))
// Soal 1

var nilai = 75;
if (nilai >= 85) {
    console.log("indeksnya A")
}
if (nilai >= 75 || nilai < 85) {
    console.log("indeksnya B")
}
if (nilai >= 65 || nilai < 75) {
    console.log("indeksnya C")
}
if (nilai >= 55 || nilai < 65) {
    console.log("indeksnya D")
}
else if (nilai < 55) {
    console.log("indeksnya E")
}

// Soal 2

var tanggal = 2;
var tahun = 1998;
var bulan = 2;
switch (bulan) {
    case 1: { console.log(tanggal, "Januari", tahun); break; }
    case 2: { console.log(tanggal, "Februari", tahun); break; }
    case 3: { console.log(tanggal, "Maret", tahun); break; }
    case 4: { console.log(tanggal, "April", tahun); break; }
    case 5: { console.log(tanggal, "Mei", tahun); break; }
    case 6: { console.log(tanggal, "Juni", tahun); break; }
    case 7: { console.log(tanggal, "Juli", tahun); break; }
    case 8: { console.log(tanggal, "Agustus", tahun); break; }
    case 9: { console.log(tanggal, "September", tahun); break; }
    case 10: { console.log(tanggal, "Oktober", tahun); break; }
    case 11: { console.log(tanggal, "November", tahun); break; }
    case 12: { console.log(tanggal, "Desember", tahun); break; }
    default: { console.log('Tidak terjadi apa-apa'); }
}


// Soal 3

function angka(deret) {
    var hasil = '';
    for (var a = 0; a < deret; a++) {
        for (var b = 0; b <= a; b++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(angka(3));

// Soal 4

for (var angka = 1; angka < 3; angka++) {
    console.log(angka + "-I love programming ");
    console.log(angka + "-I love javascript");
    console.log(angka + "-I love VueJS");
}
console.log('===');

//Saya Sangat Kurang Paham untuk soal no 4 maaf sebelumnya, Untuk Video Sessionnya saat waktu berlangsung tidak bisa diakses//


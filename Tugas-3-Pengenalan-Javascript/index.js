// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var saya = pertama.substr(0, 5)
var senang = pertama.substr(12, 7)
var belajar = kedua.substr(0, 8)
var upper = kedua.substr(8, 11).toUpperCase();

console.log(saya + senang + belajar + upper)//


// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var a = parseInt(kataPertama)
var b = parseInt(kataKedua)
var c = parseInt(kataKetiga)
var d = parseInt(kataKeempat)

console.log(a + b * c + d)


// Soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
